#!/usr/bin/env python3

# Like the linux "unzip" command except unzips all zips within the current working directory.
import os
import zipfile
files = [f for f in os.listdir(os.getcwd()) if os.path.isfile(f)]
print("Finding Zip Files.")
for file in files:
    if file[-4:] == ".zip":
        print(f"found: {file}")
        with zipfile.ZipFile(file,"r") as z:
            z.extractall(os.getcwd())
        print(f"  Unzipped {file}")
print("Unzipped all")

