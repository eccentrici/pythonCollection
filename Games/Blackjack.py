import random as r
from os import system, name
import time
# Defining possible pickups
suits = ['Spades','Hearts','Diamonds','Clubs']
cards = ['Ace',2,3,5,6,7,8,9,10,'Jack','Queen','King']
# Creating a turn counter, will be incremented and used to display current card.
def clear():
    if name == "nt":
        _ = system('cls')
    else:
        _ = system('clear')
turn = 0
# Start function, runs only on start.
def start():
        clear()
        global turn # Accessing turn
        global playerCards 
        global playerSuits
        print("You picked up:")

        playerCards = [] # Creating an empty array for holding the player's Cards.
        playerSuits = [] # Creating an empty array for holding player's cards' suits.
        
        # Generating 2 Cards, and 2 suits, appending them to the array accordingly.
        playerCards.append(r.choice(cards))
        playerCards.append(r.choice(cards))
        playerSuits.append(r.choice(suits))
        playerSuits.append(r.choice(suits))
        
        time.sleep(.3)
        # Printing what was picked up.
        print(f" {playerCards[0]} of {playerSuits[0]}.")
        time.sleep(.3)
        print(f" {playerCards[1]} of {playerSuits[1]}.")
        # Generating a total value
        print(f"Total points: {total(playerCards)}")
        # Incrementing
        turn +=1
        # Moving on.
        print('What next? (S)tand or (H)it? ')
        inp = input("> ")
        if inp.lower() == 'h':
            hit()
        elif inp.lower() == "s":
            stand()
        
# A hit function, used for giving the player more cards.    
def hit():  
        global turn
        global playerSuits
        global PlayerCards

        turn +=1 # Adding to the turn variable.
        # Generating a card and an assigned suit in the same index.
        playerCards.append(r.choice(cards)) 
        playerSuits.append(r.choice(suits))
        time.sleep(.3)
        # Pickup message
        clear()
        print("Dealer slides you a card...")
        time.sleep(.3)
        print(f"You picked up:")
        print(f" {playerCards[turn]} of {playerSuits[turn]}")
        print(f'Total points: {total(playerCards)}') # Requesting the total points.
        time.sleep(.5)
        # Reprompt
        print('What next? (S)tand or (H)it?')
        inp = input("> ")
        if inp.lower() == 'h':
            hit()
        else:
            stand()
# A stand function, used when the player wishes to hold their cards.
def stand():
    global turn
    time.sleep(.4)
    clear()
    print("You chose to stand.")
    
    # Creating empty arrays for the computer. I named them CPU like in Mario Kart :P
    time.sleep(.6)
    print("CPU's turn!")
    time.sleep(.6)
    clear()
    cpuCards = []
    cpuSuits = []

    cpuCards.append(r.choice(cards))
    cpuSuits.append(r.choice(suits))
    cpuCards.append(r.choice(cards))
    cpuSuits.append(r.choice(suits))
    time.sleep(1)
    print("CPU has: ")
    print(f' {cpuCards[0]} of {cpuSuits[0]}')
    time.sleep(.3)
    print(f' {cpuCards[1]} of {cpuSuits[1]}')
    time.sleep(.3)
    print(f'Total points: {total(cpuCards)}')

    cpuChoices = ['stand','hit']
    
    if total(cpuCards) < 15:
        choice = r.choice(cpuChoices)
        if choice == 'hit':
            time.sleep(.8)
            print("CPU has decided to hit")
            cpuCards.append(r.choice(cards))
            cpuSuits.append(r.choice(suits))
            time.sleep(1)
            print("CPU picks up: ")
            time.sleep(1)
            print(f'{cpuCards[2]} of {cpuSuits[2]}')
            time.sleep(1)
            print(f'CPU now has a total of: {total(cpuCards)}')
            time.sleep(1)
            print("CPU decided to stand")
            time.sleep(1)
            if total(cpuCards) > total(playerCards):
                  print("CPU Wins!")
            elif total(cpuCards) == total(playerCards):
                  print("Tie!")
            else:
                  print("Player Wins!")
        else:
            print("CPU has decided to stand")
            time.sleep(1)
            if total(cpuCards) > total(playerCards):
                  print("CPU Wins!")
            elif total(cpuCards) == total(playerCards):
                  print("Tie!")
            else:
                print("Player wins!")
    else:
        print("CPU has decided to stand")
        time.sleep(.8)
        if total(cpuCards) > total(playerCards):
            print("CPU Wins!")
        elif total(cpuCards) == total(playerCards):
            print("Tie!")
        else:
            print("Player wins!")
        
# A bust function, used when a player goes over the maximum amount ( 21 )
def bust(t):
    clear()
    time.sleep(.4)
    print(f"Woops, you went over 21. You lose! (You had {t})")
    os._exit(0) # exiting

# Function for generating the totals of cards
def total(c):
    # Since the arrays include strings, we will replace them with integers with the according value.
    for i in range(len(c)):
        if c[i] == 'King' or c[i] == "Queen" or c[i] == "Jack":
            c[i] = 10
        elif c[i] == "Ace":
            c[i] = 1
    
    # Turning the whole array into an integer and returning the sum.
    total = sum([int(i) for i in c])

    # Checking if player is still below the maximum, otherwise call the bust functon.
    if total < 21:
        time.sleep(.4)
        return total
    elif total == 21:
        time.sleep(.4)
        print("Hey would you look at that! You hit 21!")
    elif total > 21:
    
        bust(total)
# used for initializing
if '__main__' == __name__:
    start()

