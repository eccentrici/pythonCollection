#!/usr/bin/env python3
# Creates random stars in your terminal. ~ Sat, 19 Feb, 2022
import curses
import time
import random
from curses import wrapper
def main(stdscr):

    stdscr.clear()
    for i in range(random.randint(1,1000000)):
        time.sleep(0.1)
        rows, cols = stdscr.getmaxyx()
        a = random.randint(1, int(rows - 1))
        b = random.randint(1, int(cols - 6))
        stdscr.addstr(a, b, f"*", curses.A_BOLD)
        curses.initscr()
        stdscr.refresh()
    stdscr.getch()
wrapper(main)
