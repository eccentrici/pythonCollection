import time
from difflib import SequenceMatcher

def similar(a, b):
    return SequenceMatcher(None, a, b).ratio()



teststring = "This is a test string just type it lol"
print(teststring)
start = time.time()
inp = input("> ")
end = time.time()
duration, _0 = divmod(end-start, 1)
print(duration)
print(similar(teststring, inp))
