# Python Collection
This repository is a collection of Python Programs I've made that don't exactly have their own repo, thus this repository exists. Typically the programs in this repo are small and made for various reasons.

## Fun
* Stars.py
	* Generates random stars within the terminal. (Dynamically changes as terminal size changes).
## Utilities
* Unzipall.py
	* Unzips all files within the current directory.
## Games
* Blackjack.py
	* Classic Blackjack game against a CPU.
